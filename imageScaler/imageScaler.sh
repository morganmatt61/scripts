#!/bin/bash
# Require imagemagick installed.
# Param 1 imagesize in format 64x64.
# Ignores aspect ratio with ! in convert line.
# Currently only png and jpg, can add more in for.


imagesize=$1
for image in ./*.{jpg,png}; do
	echo "scaling $image to $imagesize"
	file $image
	convert $image -resize $imagesize\! $image
	file $image
done
